# Kubernetes Dojo
Welcome to Kubenetes 101 for developers. The aim of this dojo is to get you familiar with some key concepts so that you can start developing applications that run in a Kubernetes managed environment.

Topics include:
- Namespaces
- Pods and deployments
- Services
- Scaling deployments
- Local development and testing


# Previous Knowledge
Experience with running commands in the terminal
Basic knowledge of Docker


# Agenda​
1. Kubernetes overview
2. Running a cluster locally using Docker
3. Basic commands
4. Creating deployments and services
5. Scaling
6. Local dev with Skaffold


# Preparation

Please clone the following repository that contains examples and code for the practical</br>
https://gitlab.com/vchegwidden/kubernetes-dojo.git

You will need to install the following software to complete this dojo. Please note that the installation takes a while so please set aside some time the day before to install everything and make sure it works.

## Docker
Install docker desktop</br>
https://www.docker.com/get-started

Execute the following command to verify:
```
docker --version
```
Output should look something like this:
```
Docker version 19.03.12, build 48a66213fe
```

## Kubernetes CLI
Use this guide to install the commandline tools</br>
https://kubernetes.io/docs/tasks/tools/install-kubectl/

Execute the following command to verify:
```
kubectl version
```
Output should look something like this:
```
Client Version: version.Info{Major:"1", Minor:"16+", GitVersion:"v1.16.6-beta.0", GitCommit:"e7f962ba86f4ce7033828210ca3556393c377bcc", GitTreeState:"clean", BuildDate:"2020-01-15T08:26:26Z", GoVersion:"go1.13.5", Compiler:"gc", Platform:"darwin/amd64"}
The connection to the server localhost:8080 was refused - did you specify the right host or port?
```


## Enable Kubernetes in Docker
Open docker's settings, navigate to the Kubernetes section and enable Kubernetes.</br>
You can follow step 2 on this guide https://birthday.play-with-docker.com/kubernetes-docker-desktop/

Execute the following command to verify:
```
kubectl cluster-info
```
Output should look something like this:
```
Kubernetes control plane is running at https://kubernetes.docker.internal:6443
KubeDNS is running at https://kubernetes.docker.internal:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
```
To further debug and diagnose cluster problems, use `kubectl cluster-info dump`


## KubeMQ
Follow this guide to install KubeMQ. You will need to sign up for a free account and then follow the steps in the quickstart.
https://account.kubemq.io/home/get-kubemq/kubernetes

You will need to have Docker and Minikube running to complete the installation so start docker and then run the following before starting the installation
```
minikube start --vm-driver=docker
```

Execute the following command to verify:
```
kubectl get kubemqclusters -n kubemq
```
Output should look something like this:
```
NAME             VERSION   STATUS   REPLICAS   READY   GRPC   REST   API   LICENSE-TYPE   LICENSE-TO   LICENSE-EXPIRE
kubemq-cluster
```


## Java JDK
Install the java development kit</br>
https://adoptopenjdk.net/

Execute the following command to verify:
```
java -version
```
Output should look something like this:
```
openjdk version "12.0.2" 2019-07-16
OpenJDK Runtime Environment AdoptOpenJDK (build 12.0.2+10)
OpenJDK 64-Bit Server VM AdoptOpenJDK (build 12.0.2+10, mixed mode, sharing)
```

## Skaffold
https://skaffold.dev/


Execute the following command to verify:
```
skaffold version
```
Output should look something like this:
```
v1.12.0
```

# Next
git checkout exercise1
